
variable "Host" {
  default = "localhost"
}

variable "Port" {
  default = "80"
}

variable "ContainerPort" {
  default = "8080"
}

variable "arquivoAmbiente" {
  default = ".env-dev"
}

variable "imagem" {
  default = ""
}
